class MyrtiloElement {
  constructor(htmlElement) {
    this.htmlElement = htmlElement || null;
  }

  id() {
    return this.htmlElement.id;
  }

  attr(attributeName, data) {
    if (data) {
      this.htmlElement.setAttribute(attributeName, data);
    }
    return this.htmlElement.getAttribute(attributeName);
  }

  data(attributeName) {
    return this.htmlElement.getAttribute(`data-${attributeName}`);
  }

  html(replacementString) {
    if (replacementString) {
      this.htmlElement.innerHTML = replacementString;
    }
    return this.htmlElement.innerHTML;
  }

  on(e, callback) {
    this.htmlElement.addEventListener(e, callback);
  }

  find(query) {
    let result = this.htmlElement.querySelectorAll(query);

    if (result.length) {
      result = new MyrtiloArray([...result]);
    } else {
      result = new MyrtiloArray(null);
    }
    return result;
  }

  focus() {
    this.htmlElement.focus();
  }

  removeClass(target) {
    if (typeof(target) === 'string') {
      this.htmlElement.classList.remove(...target.split(' '));
    } else {
      // We are assuming a Regex here. An exception might be in order?
      const classes = Array.from(this.htmlElement.classList);

      for (let i = 0; i < classes.length; i += 1)
        if (target.test(classes[i]))
          this.htmlElement.classList.remove(classes[i]);
    }
  }

  addClass(classes) {
    this.htmlElement.classList.add(...classes.split(' '));
  }

  hasClass(className) {
    return this.htmlElement.classList.contains(className);
  }

  toggleClass(className) {
    this.hasClass(className) ? this.removeClass(className) : this.addClass(className);
  }

  replaceClass(from, to) {
    this.removeClass(from);
    this.addClass(to);
  }

  swapClass(a, b) {
    this.hasClass(a) ? this.replaceClass(a, b) : this.replaceClass(b, a);
  }

  value(newValue) {
    // TODO: An obvious pattern is forming, which can be abstracted.
    if (newValue) {
      this.htmlElement.value = newValue;
    }

    return this.htmlElement.value;
  }

  css(specifier, data) {
    const oldStyle = this.attr('style');

    let newStyle = `${specifier}: ${data};`;

    if (oldStyle) {
      newStyle += oldStyle
        .split(';')
        .map((s) => s.trim())
        .filter((s) => s.indexOf(specifier) !== 0)
        .join('; ');
    }

    this.htmlElement.style = newStyle;
  }

  click() {
    this.htmlElement.click();
  }

  append(string) {
    /*
    https://stackoverflow.com/questions/3039723/appending-to-innerhtml-without-rest-of-contents-flicking
    */
    this.htmlElement.appendChild(string);
  }

  y () {
    return this.htmlElement.getBoundingClientRect().y
  }

  x () {
    return this.htmlElement.getBoundingClientRect().x
  }
}
