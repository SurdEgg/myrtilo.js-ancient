/* The class which interacts with the document itself. */
class Doc {
  static get(query) {
    let list = document.querySelectorAll(query);

    if (list.length) {
      list = new MyrtiloArray([...list]);
    } else {
      list = new MyrtiloArray(null);
    }

    return list;
  }

  static id(query) {
    return new MyrtiloElement(document.getElementById(query));
  }

  static keyup(callback) {
    window.addEventListener('keyup', function(e) {
      callback(e);
    });
  }

  static ready(callback) {
    document.addEventListener('DOMContentLoaded', callback);
  }

  static escape(dirtyString) {
    return dirtyString.replace(/</g, '&lt;')
                      .replace(/>/g, '&gt;')
                      .replace(/&/g, '&amp;')
                      .replace(/"/g, '&quot;')
                      .replace(/'/g, '&#039;');
  }

  static unescape(dirtyString) {
    return cleanString.replace(/&lt;/g, '<')
                      .replace(/&gt;/g, '>')
                      .replace(/&amp;/g, '&')
                      .replace(/&quot;/g, '"')
                      .replace(/%#039/g, '\'');
  }
}
