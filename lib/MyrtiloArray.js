class MyrtiloArray extends Array {
  constructor(htmlElements) {
    super(htmlElements);

    if (htmlElements) {
      for (let i = 0; i < htmlElements.length; i += 1) {
        this[i] = new MyrtiloElement(htmlElements[i]);
      }
    }
  }

  isEmpty() {
    return (!this.first());
  }

  first() {
    return this[0];
  }

  last() {
    return this[this.length - 1];
  }

  rest() {
    return this.slice(1, );
  }

  determineType(element) {
    if (this.length === 1) {
      return element[0];
    } else {
      return element;
    }
  }

  on(e, callback) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].on(e, callback);
    }
  }

  // Methods that apply to all elements.
  // TODO this should return an array of whatever the responses would normally
  // be. Also, abstract this.

  attr(attributeName, data) {
    if (!this.isEmpty()) {
      for (let i = 0; i < this.length; i += 1) {
        this[i].attr(attributeName, data);
      }
    }
  }

  value(newValue) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].value(newValue);
    }
  }

  css(specifier, data) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].css(specifier, data);
    }
  }

  click() {
    for (let i = 0; i < this.length; i += 1) {
      this[i].click();
    }
  }

  hasClass(className) {
    let classPresent = false;

    console.log(this);
    for (let i = 0; i < this.length; i += 1) {
      if (this[i].hasClass(className)) {
        console.log(this[i]);
        classPresent = true;
      }
    }

    return classPresent;
  }

  addClass(className) {
    if (!this.isEmpty()) {
      for (let i = 0; i < this.length; i += 1) {
        this[i].addClass(className);
      }
    }
  }  

  removeClass(className) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].removeClass(className);
    }
  }

  replaceClass(from, to) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].replaceClass(from, to);
    }
  }

  swapClass(a, b) {
    for (let i = 0; i < this.length; ++i) {
      this[i].swapClass(a, b);
    }
  }

  data(attributeName) {
    let result = [];

    for (let i = 0; i < this.length; i += 1) {
      result.push(this[i].data(attributeName));
    }

    return this.determineType(result);
  }

  html(replacementString) {
    let result = [];

    for (let i = 0; i < this.length; i += 1) {
      result.push(this[i].html(replacementString));
    }

    return this.determineType(result);
  }

  append(string) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].append(string);
    }
  }
}
