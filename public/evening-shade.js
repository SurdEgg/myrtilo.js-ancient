function rand(min, max) {
    return Math.floor((Math.random() * max) + min);
}

const that = function convertHtmlElementToEveningShade(htmlElement) {
    return new EveningShadeElement(htmlElement);
};

const say = console.log
class EveningShadeElement {
  constructor(htmlElement) {
    this.htmlElement = htmlElement || null;
  }

  id() {
    return this.htmlElement.id;
  }

  attr(attributeName, data) {
    if (data) {
      this.htmlElement.setAttribute(attributeName, data);
    }
    return this.htmlElement.getAttribute(attributeName);
  }

  data(attributeName) {
    return this.htmlElement.getAttribute(`data-${attributeName}`);
  }

  html(replacementString) {
    if (replacementString) {
      this.htmlElement.innerHTML = replacementString;
    }
    return this.htmlElement.innerHTML;
  }

  on(e, callback) {
    this.htmlElement.addEventListener(e, callback);
  }

  find(query) {
    let result = this.htmlElement.querySelectorAll(query);

    if (result.length) {
      result = new EveningShadeArray([...result]);
    } else {
      result = new EveningShadeArray(null);
    }
    return result;
  }

  focus() {
    this.htmlElement.focus();
  }

  removeClass(classes) {
    this.htmlElement.classList.remove(...classes.split(' '));
  }

  addClass(classes) {
    this.htmlElement.classList.add(...classes.split(' '));
  }

  hasClass(className) {
    return this.htmlElement.classList.contains(className);
  }

  toggleClass(className) {
    this.hasClass(className) ? this.removeClass(className) : this.addClass(className);
  }

  replaceClass(from, to) {
    this.removeClass(from);
    this.addClass(to);
  }

  swapClass(a, b) {
    this.hasClass(a) ? this.replaceClass(a, b) : this.replaceClass(b, a);
  }

  value(newValue) {
    // TODO: An obvious pattern is forming, which can be abstracted.
    if (newValue) {
      this.htmlElement.value = newValue;
    }

    return this.htmlElement.value;
  }

  css(specifier, data) {
    const oldStyle = this.attr('style');

    let newStyle = `${specifier}: ${data};`;

    if (oldStyle) {
      newStyle += oldStyle
        .split(';')
        .map((s) => s.trim())
        .filter((s) => s.indexOf(specifier) !== 0)
        .join('; ');
    }

    this.htmlElement.style = newStyle;
  }

  click() {
    this.htmlElement.click();
  }

  append(string) {
    /*
    https://stackoverflow.com/questions/3039723/appending-to-innerhtml-without-rest-of-contents-flicking
    */
    this.htmlElement.appendChild(string);
  }
}
class EveningShadeArray extends Array {
  constructor(htmlElements) {
    super(htmlElements);

    if (htmlElements) {
      for (let i = 0; i < htmlElements.length; i += 1) {
        this[i] = new EveningShadeElement(htmlElements[i]);
      }
    }
  }

  isEmpty() {
    return (!this.first());
  }

  first() {
    return this[0];
  }

  last() {
    return this[this.length - 1];
  }

  rest() {
    return this.slice(1, );
  }

  determineType(element) {
    if (this.length === 1) {
      return element[0];
    } else {
      return element;
    }
  }

  on(e, callback) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].on(e, callback);
    }
  }

  // Methods that apply to all elements.
  // TODO this should return an array of whatever the responses would normally
  // be. Also, abstract this.

  attr(attributeName, data) {
    if (!this.isEmpty()) {
      for (let i = 0; i < this.length; i += 1) {
        this[i].attr(attributeName, data);
      }
    }
  }

  value(newValue) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].value(newValue);
    }
  }

  css(specifier, data) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].css(specifier, data);
    }
  }

  click() {
    for (let i = 0; i < this.length; i += 1) {
      this[i].click();
    }
  }

  hasClass(className) {
    let classPresent = false;

    console.log(this);
    for (let i = 0; i < this.length; i += 1) {
      if (this[i].hasClass(className)) {
        console.log(this[i]);
        classPresent = true;
      }
    }

    return classPresent;
  }

  addClass(className) {
    if (!this.isEmpty()) {
      for (let i = 0; i < this.length; i += 1) {
        this[i].addClass(className);
      }
    }
  }  

  removeClass(className) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].removeClass(className);
    }
  }

  replaceClass(from, to) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].replaceClass(from, to);
    }
  }

  swapClass(a, b) {
    for (let i = 0; i < this.length; ++i) {
      this[i].swapClass(a, b);
    }
  }

  data(attributeName) {
    let result = [];

    for (let i = 0; i < this.length; i += 1) {
      result.push(this[i].data(attributeName));
    }

    return this.determineType(result);
  }

  html(replacementString) {
    let result = [];

    for (let i = 0; i < this.length; i += 1) {
      result.push(this[i].html(replacementString));
    }

    return this.determineType(result);
  }

  append(string) {
    for (let i = 0; i < this.length; i += 1) {
      this[i].append(string);
    }
  }
}
class Doc {
  static get(query) {
    let list = document.querySelectorAll(query);

    if (list.length) {
      list = new EveningShadeArray([...list]);
    } else {
      list = new EveningShadeArray(null);
    }

    return list;
  }

  static keyup(func) {
    window.addEventListener('keyup', function(e) {
      func(e);
    });
  }

  static ready(callback) {
    document.addEventListener('DOMContentLoaded', callback);
  }

  static escape(dirtyString) {
    return dirtyString
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/&/g, '&amp;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#039;');
  }
}
