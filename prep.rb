#!/usr/bin/env ruby

require 'listen'
require 'uglifier'

path = Dir.pwd + '/lib/'
cleaner = /(^\.|.swp$)/
files = ['globals',
         'MyrtiloElement',
         'MyrtiloArray',
         'Doc'].map { |f| f + '.js' }

if ARGV.first == '-u'
  result = files.map { |f| f = File.read(path + f) }.join('')
  File.open(Dir.pwd + '/public/myrtilo.js', 'w') { |f| f.write result }
  result = Uglifier.new(harmony: true).compile(result)
  File.open(Dir.pwd + '/myrtilo.js', 'w') { |f| f.write result }
else
  listener = Listen.to(path, ignore: /(^\.|\.swp$)/) do |modified, added, removed|
    puts "Change to #{modified.first} detected. Reparsing"
    result = files.map { |f| f = File.read(path + f) }.join('')
    File.open(Dir.pwd + '/public/myrtilo.js', 'w') { |f| f.write result }
  end

  puts 'Listening...'
  listener.start
  sleep
end
